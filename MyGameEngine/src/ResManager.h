#ifndef RES_MANAGER_H
#define RES_MANAGER_H

#include <map>

#include "graphics\Shader.h"
#include "graphics\stb_image.h"
#include "graphics\Texture.h"

class ResourceManager {
public:
	static std::map<std::string, Shader*> registeredShaders;
	static std::map<std::string, Texture*> registeredTextures;

	// Shaders location
	static const std::string shaderLoc;
	// Texture location
	static const std::string texLoc;

private:
	//static bool isInstantiated; // maybe this whole class should be singleton...

public:
	ResourceManager();
	~ResourceManager();

	static void UpdateResources();
	static bool ReadFromFile(std::string path, std::string& buffer);
    static Shader* GetShaderByName(std::string name);
    static Texture* GetTextureByName(std::string name);

private:
	static std::string firstLevelData;
};

#endif // !RES_MANAGER_H
