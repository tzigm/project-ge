#include "PhysicsEngine.h"

#include <cmath>
#include <iostream>

#include "../ResManager.h"
#include "GraphicsEngine.h"

PhysicsEngine::PhysicsEngine() {
    std::cout << "Physics Engine is being initialized." << std::endl;
}

PhysicsEngine::~PhysicsEngine() {
    // if object is added to GraphicsEngine Renderable items lists - that list will delete these objects.
    /*delete m_testingGO;
    delete m_testingGO2;*/
}

void PhysicsEngine::InitItens()
{
    m_testingGO = new GameObject();
    m_testingGO2 = new GameObject();
    m_testingGO->SetMaterial(ResourceManager::GetShaderByName("quadWithTex"), ResourceManager::GetTextureByName("uvteximg"));
    m_testingGO2->SetMaterial(ResourceManager::GetShaderByName("quadWithTex"), ResourceManager::GetTextureByName("pepe"));
    m_testingGO->transform.scale = Vector3(0.5f, 0.5f, 1.0f);
    m_testingGO2->transform.scale = Vector3(0.5f, 0.5f, 1.0f);
    GraphicsEngine::AddRenderableItem(m_testingGO);
    GraphicsEngine::AddRenderableItem(m_testingGO2);
}

float x = 0, y = 0;
void PhysicsEngine::Update() {
    float x_sin = sinf(x+=0.0025f);
    float y_cos = cosf(y+=0.0025f);
    Vector3 np(x_sin, y_cos, 0.0f);
    Vector3 np2(-x_sin, -y_cos, 0.0f);

    m_testingGO->transform.position = np;
    m_testingGO2->transform.position = np2;
}