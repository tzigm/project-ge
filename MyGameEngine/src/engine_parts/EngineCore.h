#ifndef ENGINE_CORE_H
#define ENGINE_CORE_H

// includes
#include "PhysicsEngine.h"
#include "GraphicsEngine.h"
#include "../ResManager.h"
#include "../utils/GameObject.h"

// Core of the engine.
class EngineCore {
public:
	enum GameState {
		STARTING,
		MAIN_MENU,
		IN_GAME_LVL1,
		IN_GAME_LVL2,
		FINISHED
	};
	// Game's current (global) state.
	static GameState currentGameState;

public:
	EngineCore();
	~EngineCore();

	// Initializes whole engine
	void Init();

private:
	PhysicsEngine *physicsEngine;
	GraphicsEngine *graphicsEngine;
	// Main game loop.
	void Update();
	// Stops game
	void Stop(); // TODO: Implement game stop
};

#endif // !H_ENGINE_CORE

