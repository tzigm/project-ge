#ifndef GRAPH_ENG_H
#define GRAPH_ENG_H

// Includes
#include <glad\glad.h>
#include <GLFW\glfw3.h>
#include <vector>

#include "../graphics/Shader.h"
#include "../graphics/Texture.h"
#include "../graphics/Renderable.h"


class GraphicsEngine {
private:
	// General window attributes

	char* name;
	int width, height;
	GLFWwindow* window;
    static std::vector<Renderable*> itemsToRender;
	// TODO: Maybe I should consider on priority when rendering list of objects...
	Shader* shaderObj;
	bool shaderIsUpToDate;

public:
	// Empty constructor. Initializes window with 800x600 resolution and "BLANK_NAME" name
	GraphicsEngine();
	// Constructor which initializes window with given width, height and name.
	GraphicsEngine(int w_width, int w_height, char* w_name);
	~GraphicsEngine();

	// Main loop which updates window (should and is called from main game loop inside EngineCore class)
	void UpdateWindow();
	// Stops rendering
	void Terminate();
    
    static void AddRenderableItem(Renderable* r);


private:
	int Init();
    void RenderAllObjects();

};



#endif // !GRAPH_ENG_H

