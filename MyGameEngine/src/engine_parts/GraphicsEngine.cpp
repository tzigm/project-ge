#include "GraphicsEngine.h"
#include "EngineCore.h"

#include <iostream>

// Functions
void CB_FramebufferResize(GLFWwindow* window, int width, int height);
void CB_WindowClose(GLFWwindow *window);
void HelloTriangle(GLFWwindow *window);
void HelloQuad(GLFWwindow *window);
void HelloTexture(GLFWwindow *window);

std::vector<Renderable*> GraphicsEngine::itemsToRender;

GraphicsEngine::GraphicsEngine() {
	name = "BLANK_NAME";
	width = 800;
	height = 600;
	Init();
}

GraphicsEngine::GraphicsEngine(int w_width, int w_height, char* w_name ) {
	name = w_name;
	width = w_width;
	height = w_height;
	Init();
}

GraphicsEngine::~GraphicsEngine() {
	std::cout << "Graphics engine is being destroyed...";
    for (int i = itemsToRender.size() - 1; i >= 0; i--)
    {
        delete itemsToRender[i];
    }
    itemsToRender.clear();
}

int GraphicsEngine::Init() {
	std::cout << "Graphics engine is being initialized" << std::endl;

	if (!glfwInit()) {
		std::cout << "GLFW initialization failed." << std::endl;
		return -1;
	}
	// creates window here
	window = glfwCreateWindow(width, height, name, NULL, NULL);
	if (!window) {
		std::cout << "GLFW window creation failed." << std::endl;
		Terminate();
		return -1;
	}

	glfwMakeContextCurrent(window);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}
	glViewport(0, 0, width, height);

	//HelloTriangle(window);
	//HelloQuad(window);
	//HelloTexture(window);

	// TODO: Set callbacks here
	glfwSetWindowCloseCallback(window, CB_WindowClose);
	glfwSetFramebufferSizeCallback(window, CB_FramebufferResize);


	return 1;
}

void GraphicsEngine::UpdateWindow() {
	// Render here
	glClearColor(0.25f, 0.3f, 0.25f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	//glDrawArrays(GL_TRIANGLES, 0, 3);
	//glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    RenderAllObjects();

	glfwSwapBuffers(window);
	glfwPollEvents();
}

void GraphicsEngine::RenderAllObjects()
{
    for (int i = 0; i < itemsToRender.size(); i++)
    {
        itemsToRender[i]->Draw();
    }
}

void GraphicsEngine::AddRenderableItem(Renderable* r)
{
    itemsToRender.push_back(r);
}

void GraphicsEngine::Terminate() {
	glfwTerminate();
}

void HelloTriangle(GLFWwindow *window) {

	// in tutorial it is drawn this way: lb -> rb -> tu
	float vertices[] = {
		0.0f,  0.5f,  0.0f,
		-0.5f, -0.5f,  0.0f,
		0.5f, -0.5f,  0.0f
	};
	unsigned int VBO, VAO; // vertex buffer object
	glGenBuffers(1, &VBO);
	glGenVertexArrays(1, &VAO);

	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	ResourceManager::UpdateResources();

	std::map<std::string, Shader*>::iterator it;
	it = ResourceManager::registeredShaders.find("simpleShader");
	if (it != ResourceManager::registeredShaders.end()) {
		it->second->Use();
	}

	// TODO:
	// Make VAO and VBO global scope, so class can delete/update them.
	// Move shader creation into another class
	// Read shaders from files
}

void HelloQuad(GLFWwindow *window) {
	// positions			// colors			// texture coords
	float vertices[] = {
		0.5f, 0.5f, 0.0f,
		0.5f, -0.5f, 0.0f,
		-0.5f, -0.5f, 0.0f,
		-0.5f, 0.5f, 0.0f
	};
	unsigned int indices[] = {
		0, 1, 3,
		1, 2, 3
	};

	unsigned int VBO, EBO, VAO;
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);
	glGenVertexArrays(1, &VAO);

	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

    ResourceManager::UpdateResources();

	std::map<std::string, Shader*>::iterator it;
	it = ResourceManager::registeredShaders.find("simpleShader");
	if (it != ResourceManager::registeredShaders.end()) {
		it->second->Use();
	}

}

void HelloTexture(GLFWwindow *window) {
	// positions				// colors			// texture coords
	float vertices[] = {
		 0.5f,  0.5f, 0.0f,		1.0f, 1.0f, 1.0f,	1.0f, 1.0f,
		 0.5f, -0.5f, 0.0f,		1.0f, 1.0f, 1.0f,	1.0f, 0.0f,
		-0.5f, -0.5f, 0.0f,		1.0f, 1.0f, 1.0f,	0.0f, 0.0f,
		-0.5f,  0.5f, 0.0f,		1.0f, 1.0f, 1.0f,	0.0f, 1.0f
	};
	unsigned int indices[] = {
		0, 1, 3,
		1, 2, 3
	};

	unsigned int VBO, EBO, VAO;
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);

	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	// position attribute
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	// color attribute
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);
	// texture coord attribute
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
	glEnableVertexAttribArray(2);



	ResourceManager::UpdateResources();

	std::map<std::string, Texture*>::iterator itTex;
	itTex = ResourceManager::registeredTextures.find("uvteximg");
	if (itTex != ResourceManager::registeredTextures.end()) {
		itTex->second->Bind();
	}
	else {
		std::cout << "Texture was not found." << std::endl;
	}

	std::map<std::string, Shader*>::iterator it;
	it = ResourceManager::registeredShaders.find("quadWithTex");
	if (it != ResourceManager::registeredShaders.end()) {
		it->second->Use();
	}
	else {
		std::cout << "Shader was not found." << std::endl;
	}


	glBindVertexArray(VAO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

}


////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Callbacks


void CB_WindowClose(GLFWwindow* window) {
	std::cout << "Closing window." << std::endl;
	EngineCore::currentGameState = EngineCore::GameState::FINISHED;
}

void CB_FramebufferResize(GLFWwindow* window, int width, int height)
{
	// make sure the viewport matches the new window dimensions; note that width and 
	// height will be significantly larger than specified on retina displays.
	glViewport(0, 0, width, height);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
