#include "EngineCore.h"

#include <iostream>
#include "../utils/Timer.h"

EngineCore::GameState EngineCore::currentGameState = EngineCore::GameState::STARTING;

EngineCore::EngineCore() {

}

EngineCore::~EngineCore() {
	delete physicsEngine;
	delete graphicsEngine; 
}

void EngineCore::Init() {
	// For future self:
	// May consider putting these two engines into separate threads...
	currentGameState = EngineCore::GameState::IN_GAME_LVL1;
	graphicsEngine = new GraphicsEngine(800, 600, "My first window"); // Will create window and only then do other stuff
	physicsEngine = new PhysicsEngine();

	if (!graphicsEngine || !physicsEngine) {
		std::cout << "Failed to initialize graphics or physics engine.";
		return; 
	}

    ResourceManager::UpdateResources();
    physicsEngine->InitItens();

	// For testing always setting starting state as Level 1
	// Might do some more checks here

	Update(); // Starting game loop
}

void EngineCore::Update() {
    int frames = 0;
    int updates = 0;
    using namespace std::chrono;
    milliseconds ms;
	while (currentGameState != EngineCore::GameState::FINISHED) {
		physicsEngine->Update();
		graphicsEngine->UpdateWindow();
        std::cout << Timer::GetCurrentMs() << std::endl;
	}
	graphicsEngine->Terminate();
}