#include "Texture.h"

#include <iostream>

Texture::Texture():
	m_width(0), m_height(0), wrap_S(GL_REPEAT), wrap_T(GL_REPEAT), minFilterMode(GL_LINEAR), maxFilterMode(GL_LINEAR)
{
	glGenTextures(1, &m_textureID);
}

Texture::~Texture() {

}

void Texture::Generate(int width, int height, unsigned char* data) {

	m_width = width;
	m_height = height;
	glBindTexture(GL_TEXTURE_2D, m_textureID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap_S);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap_T);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minFilterMode);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, maxFilterMode);

	glGenerateMipmap(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, 0); // unbinding
}

void Texture::Bind() const {
	glBindTexture(GL_TEXTURE_2D, m_textureID);
}
