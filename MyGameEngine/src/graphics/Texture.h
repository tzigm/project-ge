#ifndef TEXTURE_H
#define TEXTURE_H

#include <glad/glad.h>

class Texture {
private:
	unsigned int m_textureID;
	int m_width;
	int m_height;
	//int nrChannels; cahnnels are unused atm
	GLuint wrap_S;
	GLuint wrap_T;
	GLuint minFilterMode;
	GLuint maxFilterMode;
	//float texCoords;
	
public:
	Texture();
	~Texture();
	void Bind() const;
	void Generate(int width, int height, unsigned char* data);
};

#endif // !TEXTURE_H
