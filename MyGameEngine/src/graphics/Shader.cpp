#include "Shader.h"

#include <iostream>
#include <string>

Shader::Shader(const char* vertexData, const char* fragmentData) {
	
	unsigned int vert, frag;

	// Loading both shader datas
	vert = glCreateShader(GL_VERTEX_SHADER);
	std::cout << "vert: " << vert << "; ";
	glShaderSource(vert, 1, &vertexData, NULL);
	glCompileShader(vert);
	CheckForErrors(vert, GL_COMPILE_STATUS);

	frag = glCreateShader(GL_FRAGMENT_SHADER);
	std::cout << "frag: " << frag << "; ";
	glShaderSource(frag, 1, &fragmentData, NULL);
	glCompileShader(frag);
	CheckForErrors(frag, GL_COMPILE_STATUS);
	
	// Create shader program
	ID = glCreateProgram();
	std::cout << "ID: " << ID << std::endl;
	glAttachShader(ID, vert);
	glAttachShader(ID, frag);
	glLinkProgram(ID);
	CheckForErrors(ID, GL_LINK_STATUS);

	glDeleteShader(vert);
	glDeleteShader(frag);

}

Shader::~Shader() {

}

void Shader::Use() {
	glUseProgram(ID);
}

bool Shader::CheckForErrors(const unsigned int objectID, GLenum type) {
	int success;
	char logBuffer[512];
	char* action;

	if (type == GL_COMPILE_STATUS) {
		action = "Shader compilation";
	}
	else if (type == GL_LINK_STATUS) {
		action = "Linking";
	}

	glGetProgramiv(objectID, type, &success);
	if (!success) {
		glGetProgramInfoLog(ID, 512, NULL, logBuffer);
		std::cout << "ERROR: " << action << " has failed:\n" << logBuffer << std::endl;
		return true;
	}
	return false;
}

void Shader::SetBool(const std::string &name, bool value) const
{
    glUniform1i(glGetUniformLocation(ID, name.c_str()), (int)value);
}
void Shader::SetInt(const std::string &name, bool value) const
{
    glUniform1i(glGetUniformLocation(ID, name.c_str()), value);
}
void Shader::SetFloat(const std::string &name, bool value) const
{
    glUniform1f(glGetUniformLocation(ID, name.c_str()), value);
}