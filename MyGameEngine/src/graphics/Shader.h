#ifndef SHADER_H
#define SHADER_H

#include <glad\glad.h>

#include <string>

class Shader {
public:
	unsigned int ID;

public:
	Shader(const char* vertexData, const char* fragmentData);
	~Shader();
	
	// Activates shader
	void Use();
    // Setting uniform values
    void SetBool(const std::string &name, bool value) const;
    void SetInt(const std::string &name, bool value) const;
    void SetFloat(const std::string &name, bool value) const;
private:

	// Prints error (if found)
	// Parameters: objectID opengl object ID
	// type - what was compilation was (GL_COMPILE_STATUS / GL_LINK_STATUS)
	// returns whether or compilation has errors
	bool CheckForErrors(const unsigned int objectID, GLenum type);
};

#endif // SHADER_H