#ifndef RENDERABLE_H
#define RENDERABLE_H

#include "Shader.h"
#include "Texture.h"


class Renderable
{
protected:
    Shader* m_shader;
    Texture* m_texture;
    unsigned int quadVAO;
    // Methods
public:
    // Pure virtual because I want to access GameObject's position, scale and rotation directly.
    virtual void Draw() = 0;
    
protected:
    void InitRenderer();
};

#endif // !RENDERABLE_H
