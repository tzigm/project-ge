#ifndef TRANSFORM_H
#define TRANSFORM

#include "math\Vector3.h"
#include <glm.hpp>

class Transform
{
private:
    glm::mat4 m_trans;

public:
    Vector3 position;
    Vector3 scale;
    Vector3 rotation;
private:
    // This task is quite interesting. IDK if glm has function which sets the possition so I will implement my own way
    // When we would set new position for our object it will move, rotate and scale only by delta of new and old values
    Vector3 old_position;
    Vector3 old_scale;
    Vector3 old_rotation;

public:
    Transform() : Transform(Vector3::Zero(), Vector3::Zero(), Vector3::One()) {}
    Transform(Vector3 position_) : Transform(position_, Vector3::Zero(), Vector3::One()) {}
    Transform(Vector3 position_, Vector3 rotation_) : Transform(position_, rotation_, Vector3::One()) {}
    Transform(Vector3 position_, Vector3 rotation_, Vector3 scale_);
    ~Transform();

    void Rotate(Vector3& axis, float angleDeg);
    void Rotate(float angleDeg_);
    void Scale(Vector3& scale_);
    void Move(Vector3& newPos_);

    void UpdateLocalRotation(Vector3& axis, float degree);
    void UpdateLocalPosition(Vector3& newPos);
    void UpdateLocalScale(Vector3& newScale);

    void Sync();

    glm::mat4& GetTransformationMatrix();
};

#endif // !TRANSFORM_H
