#ifndef TIMER_H
#define TIMER_H

#include <chrono>
using namespace std::chrono;

class Timer
{
private:
    static milliseconds ms;
public:
    Timer();
    ~Timer();

    static float GetCurrentMs();
    
};
#endif // !TIMER_H
