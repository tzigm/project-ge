#ifndef VECTOR3_H
#define VECTOR3_H

class Vector3
{
public:
	float x;
	float y;
	float z;

public:
    Vector3() : Vector3(0, 0, 0) {}
    Vector3(float x_) : Vector3(x_, 0, 0) {}
	Vector3(float x_, float y_) : Vector3(x_, y_, 0) {}
	Vector3(float, float, float);
	~Vector3();

	// Returns vector's magnitude
	float Magnitude() const;
	// Returns vector's magnitude with pow. of 2 (this function does not calculate sqr root)
	float SqMagnitude() const;

	static float DotProd(const Vector3&, const Vector3&);
	static float Angle(const Vector3&, const Vector3&);

    // returns Vector3(0, 0, 0)
    static Vector3 Zero();
    // returns Vector3(1, 1, 1)
    static Vector3 One();
    // returns Vector3(0, 1, 0)
    static Vector3 Up();
    // returns Vector3(0, -1, 0)
    static Vector3 Down();
    // returns Vector3(-1, 0, 0)
    static Vector3 Left();
    // returns Vector3(1, 0, 0)
    static Vector3 Right();
    // returns Vector3(0, 0, 1)
    static Vector3 Forward();
    // returns Vector3(0, 0, -1)
    static Vector3 Backward();


	Vector3 operator +(const Vector3&);
    Vector3& operator +=(const Vector3&);
	Vector3 operator -(const Vector3&);
    Vector3& operator -=(const Vector3&);

	Vector3 operator *(const int&);
	Vector3 operator *(const float&);

    Vector3& operator =(const Vector3&);

    bool operator ==(const Vector3&);
    bool operator !=(const Vector3&);
	// Add some function to it.
};

#endif // !VECTOR3_H
