#include "Vector3.h"
#include <cmath>

Vector3::Vector3(float x_, float y_, float z_) {
	x = x_;
	y = y_;
	z = z_;
}

Vector3::~Vector3() {

}

float Vector3::Magnitude() const {
	return sqrt((x * x + y * y + z * z));
}

float Vector3::SqMagnitude() const {
	return (x * x + y * y + z * z);
}

float Vector3::DotProd(const Vector3& lhs, const Vector3& rhs) {
	return ((lhs.x * rhs.x) +
		(lhs.y * rhs.y) +
		(lhs.z * rhs.z));
}

float Vector3::Angle(const Vector3& lhs, const Vector3& rhs) {
	// DotProd = |lhs| * |rhs| * cos(a)
	float dotProd = DotProd(lhs, rhs);
	float cosAn = dotProd / (lhs.Magnitude() * rhs.Magnitude());
	return acos(cosAn);
}



Vector3 Vector3::Zero()
{
    return Vector3(0.0f, 0.0f, 0.0f);
}
Vector3 Vector3::One()
{
    return Vector3(1.0f, 1.0f, 1.0f);
}
Vector3 Vector3::Up()
{
    return Vector3(0.0f, 1.0f, 0.0f);
}
Vector3 Vector3::Down()
{
    return Vector3(0.0f, -1.0f, 0.0f);
}
Vector3 Vector3::Left()
{
    return Vector3(-1.0f, 0.0f, 0.0f);
}
Vector3 Vector3::Right()
{
    return Vector3(1.0f, 0.0f, 0.0f);
}
Vector3 Vector3::Forward()
{
    return Vector3(0.0f, 0.0f, 1.0f);
}
Vector3 Vector3::Backward()
{
    return Vector3(0.0f, 0.0f, -1.0f);
}


Vector3 Vector3::operator +(const Vector3& rhs) {
	return Vector3(
		this->x + rhs.x,
		this->y + rhs.y,
		this->z + rhs.z
	);
}

Vector3& Vector3::operator +=(const Vector3& rhs)
{
    this->x += rhs.x;
    this->y += rhs.y;
    this->z += rhs.z;
    return *this;
}

Vector3 Vector3::operator -(const Vector3& rhs) {
	return Vector3(
		this->x - rhs.x,
		this->y - rhs.y,
		this->z - rhs.z
	);
}

Vector3& Vector3::operator -=(const Vector3& rhs)
{
    this->x -= rhs.x;
    this->y -= rhs.y;
    this->z -= rhs.z;
    return *this;
}

Vector3 Vector3::operator *(const int& rhs) {
	return Vector3(this->x * rhs,
		this->y * rhs,
		this->z * rhs);
}

Vector3 Vector3::operator *(const float& rhs) {
	return Vector3(this->x * rhs,
		this->y * rhs,
		this->z * rhs);
}

Vector3& Vector3::operator =(const Vector3& rhs)
{
    this->x = rhs.x;
    this->y = rhs.y;
    this->z = rhs.z;
    return *this;
}

bool Vector3::operator ==(const Vector3& rhs)
{
    if (x != rhs.x) { return false; }
    if (y != rhs.y) { return false; }
    if (z != rhs.z) { return false; }
    return true;
}

bool Vector3::operator !=(const Vector3& rhs)
{
    return !(*this == rhs);
}