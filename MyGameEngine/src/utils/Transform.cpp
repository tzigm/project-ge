#include "Transform.h"

#include <gtc/matrix_transform.hpp>

Transform::Transform(Vector3 position_, Vector3 rotation_, Vector3 scale_)
{
    position = position_;
    rotation = rotation_;
    scale = scale_;
    
    old_position = position;
    old_rotation = rotation;
    old_scale = scale;
}
Transform::~Transform()
{

}

void Transform::Rotate(Vector3& axis, float angleDeg)
{
    //UpdateLocalRotation(axis, angleDeg);
    float radians = (glm::pi<float>() / 180) * angleDeg;
    m_trans = glm::rotate(m_trans, radians, glm::vec3(axis.x, axis.y, axis.z));
}
void Transform::Rotate(float angleDeg)
{
    Rotate(Vector3::Forward(), angleDeg);
}
void Transform::Scale(Vector3& scale_)
{
    //UpdateLocalScale(scale_);
    m_trans = glm::scale(m_trans, glm::vec3(scale_.x, scale_.y, scale_.z));
}
void Transform::Move(Vector3& newPos)
{
    //UpdateLocalPosition(newPos);
    m_trans = glm::translate(m_trans, glm::vec3(newPos.x, newPos.y, newPos.z));
}

void Transform::Sync()
{
    Move(position - old_position);
    UpdateLocalPosition(position);

    if (old_scale != scale)
    {
        Vector3 sc = Vector3(
            scale.x / old_scale.x,
            scale.y / old_scale.y,
            scale.z / old_scale.z 
        );
        Scale(sc);
        old_scale = scale;
    }
}

glm::mat4& Transform::GetTransformationMatrix()
{
    return m_trans;
}


void Transform::UpdateLocalRotation(Vector3& axis, float degree)
{
    if (axis.x > 0) { rotation.x += degree; }
    if (axis.y > 0) { rotation.y += degree; }
    if (axis.z > 0) { rotation.z += degree; }
}
void Transform::UpdateLocalPosition(Vector3& newPos)
{
    //position = position + newPos;
    old_position = newPos;
}
void Transform::UpdateLocalScale(Vector3& newScale)
{
    //scale = scale + newScale;
    old_scale = newScale;
}

