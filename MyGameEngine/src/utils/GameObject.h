#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include "../graphics/Renderable.h"
#include "Transform.h"


class GameObject : public Renderable {
public:
    Transform transform;

public:
	GameObject();
	// TODO: overload constructor with one which will accept transformation class and set it for pos, scale and rotation.
	~GameObject();

	void SetMaterial(Shader *shader, Texture *texture);

	void Update(); // Should be pure virtual later
    void Draw();

};

#endif // !GAMEOBJECT_H
