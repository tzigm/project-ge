#include "GameObject.h"

#include <iostream>
#include <gtc/type_ptr.hpp>

GameObject::GameObject() {
	m_shader = nullptr;
	m_texture = nullptr;
}

GameObject::~GameObject() {

}

void GameObject::SetMaterial(Shader *shader, Texture *texture) {
	m_shader = shader;
	m_texture = texture;
    InitRenderer();
}

void GameObject::Draw()
{
    transform.Sync();
    m_shader->Use();

    unsigned int transformLoc = glGetUniformLocation(m_shader->ID, "transform");
    glUniformMatrix4fv(transformLoc, 1, GL_FALSE, glm::value_ptr(transform.GetTransformationMatrix()));

    glActiveTexture(GL_TEXTURE0);
    m_texture->Bind();
    glBindVertexArray(quadVAO);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);


    glBindVertexArray(0);
}

