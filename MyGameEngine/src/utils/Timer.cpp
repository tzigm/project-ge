#include "Timer.h"

milliseconds Timer::ms;

Timer::Timer()
{

}

Timer::~Timer()
{

}

float Timer::GetCurrentMs()
{
    ms = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
    return ms.count() % 10000;
}