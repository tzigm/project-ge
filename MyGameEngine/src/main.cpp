#include <iostream>

#include "engine_parts\EngineCore.h"

int main() {
	// Might start with some checks out there.

	std::cout << "Initializing EngineCore" << std::endl;

	EngineCore* core = new EngineCore();
	core->Init();

	// Game loop begins

	delete core;
	
	std::cout << "Program exited native way" << std::endl; 
	return 0;
}