#include "ResManager.h"
// System includes:
#include <iostream>
#include <fstream>
#include <string>
// Additional includes:
#include "engine_parts\EngineCore.h"

// Function definitions:

// Updates shaders accordingly to game state.
void CreateShaders();
void CreateTextures(std::string texName);
void UpdateShaders();
void UpdateTextures();
// What type of file should ResManager read

// Variable definitions:
std::map<std::string, Shader*> ResourceManager::registeredShaders;
std::map<std::string, Texture*> ResourceManager::registeredTextures;
const std::string shaderLoc = "resources\\graphics\\shaders\\";
const std::string texLoc = "resources\\graphics\\textures\\";

ResourceManager::ResourceManager() {

}

ResourceManager::~ResourceManager() {

}

void ResourceManager::UpdateResources() {
	UpdateShaders();
	UpdateTextures();
}

bool ResourceManager::ReadFromFile(std::string path, std::string& buffer) {
	std::ifstream sFile;
	sFile.open(path);
	if (!sFile) {
		std::cout << "Failed opening: " << path << std::endl;
		return false;
	}
	//	else 
	std::cout << "Reading has been started (" << path << ")";
	std::string line;
	while (std::getline(sFile, line)) {
		buffer += line + std::string(1, '\n');
	}
	std::cout << ".....Reading was finished" << std::endl;
	sFile.close();
	return true;
}

Shader* ResourceManager::GetShaderByName(std::string name)
{
    std::map<std::string, Shader*>::iterator it;
    it = ResourceManager::registeredShaders.find(name);
    if (it != ResourceManager::registeredShaders.end())
    {
        return it->second;
    }
    //else
    return nullptr;
}

Texture* ResourceManager::GetTextureByName(std::string name)
{
    std::map<std::string, Texture*>::iterator it;
    it = ResourceManager::registeredTextures.find(name);
    if (it != ResourceManager::registeredTextures.end())
    {
        return it->second;
    }
    //else
    return nullptr;
}

void UpdateShaders() {
	// TODO:	Should check if all registered shaders can be removed.
	//			For now cleaing all of them (although, this map will be empty)
	ResourceManager::registeredShaders.clear();

	switch (EngineCore::currentGameState) {
		// To try things out I am creating only one state
		case EngineCore::GameState::IN_GAME_LVL1 :
			std::cout << "Started updating shaders for IN_GAME_LVL1 state." << std::endl;
			// Trying out with one file.
			// Later will have to figure out how 
			CreateShaders();
		break;
	}

	std::cout << "Shaders have been updated:" << std::endl;
	std::cout << "Loaded shaders: " << ResourceManager::registeredShaders.size() << std::endl;
}

void UpdateTextures() {
	// TODO:	Should check if all registered shaders can be removed.
	//			For now cleaing all of them (although, this map will be empty)
	ResourceManager::registeredTextures.clear();

	switch (EngineCore::currentGameState) {
		// To try things out I am creating only one state
		case EngineCore::GameState::IN_GAME_LVL1:
			std::cout << "Started updating textures for IN_GAME_LVL1 state." << std::endl;
			CreateTextures("uvteximg");
            CreateTextures("pepe");
		break;
	}
}

void CreateShaders() {
	// Shaders are inside Debug folder.
	// This was set up inside ProjectProperties - Working Directory.
	std::string vertData;
	std::string fragData;

	// Reading simple shaders
	/*ResourceManager::ReadFromFile(shaderLoc + "simple.ves", vertData);
	ResourceManager::ReadFromFile(shaderLoc + "simple.frs", fragData);

	ResourceManager::registeredShaders.insert(
		std::pair<std::string, Shader*>("simpleShader", new Shader(vertData.c_str(), fragData.c_str())) 
		// Most likely conversation from std::string to c_str can be avoided :thinking:
	);*/
	
	ResourceManager::ReadFromFile(shaderLoc + "quadWithTex.ves", vertData);
	ResourceManager::ReadFromFile(shaderLoc + "quadWithTex.frs", fragData);
	ResourceManager::registeredShaders.insert(
		std::pair<std::string, Shader*>("quadWithTex", new Shader(vertData.c_str(), fragData.c_str()))
	);

}

void CreateTextures(std::string texName) {
	int width, height, nrChannels;

	//stbi_set_flip_vertically_on_load(true);
	unsigned char* data = stbi_load((texLoc + texName + ".jpg").c_str(), &width, &height, &nrChannels, 0);
	
	if (data) {
		std::cout << "Image \"" << texName << "\" was read successfully.";

		Texture* tex = new Texture();
		tex->Generate(width, height, data);

		ResourceManager::registeredTextures.insert(
			std::pair<std::string, Texture*>(texName, tex)
		);

		std::cout << "...Texture has been generated." << std::endl;
	}
	else {
		std::cout << "Failed to load texture \"" << texName << "\" (image)." << std::endl;
	}
	stbi_image_free(data);
}
