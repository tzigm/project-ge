$ git branch -d branch_name                     - Deletes local branch
$ git push origin --delete the_remote_branch    - Deletes remote branch (on server)
$ git rm -r --cached <folder>                   - Updates (removes/creates) cached data. Usefull when adding gitignore lines in mid production