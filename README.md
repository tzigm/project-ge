# Add some info about the project here later...
... But for now, I'll post some useful links
# Notes to self and others
All notes related to engine are stored here: [OneNote (requires login)](https://onedrive.live.com/edit.aspx?resid=B3323D84F4A73236!6428&cid=b3323d84f4a73236&app=OneNote) / [Read Only version](https://1drv.ms/o/s!AjYyp_SEPTKzshzFCQ8MqeqwzhcK)

Documentation [View + comment only](https://docs.google.com/document/d/1r_iBItKIDZ8RGngGYrscAZj9sn8Gl072wqXE4icwlXQ/edit?usp=sharing)

#GLFW stuff
GLFW docs: [Link (quick intro)](http://www.glfw.org/docs/latest/quick.html)

Another cool source: [Link to quick tutorial](https://learnopengl.com/#!Getting-started/Creating-a-window)